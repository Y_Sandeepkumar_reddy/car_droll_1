
// A function that returns an array of car years from the inventory array
function problem_4(inventory)
{
  const carYear = [];
  for(let index = 0; index < inventory.length; index++)
  {
    carYear.push(inventory[index].car_year)
  }
  return carYear;
}
module.exports = problem_4;