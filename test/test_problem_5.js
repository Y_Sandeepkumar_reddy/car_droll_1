const inventory = require("../inventory/inventory");
const problem_4 = require("../problems/problem_4");
const problem_5 = require("../problems/problem_5");

const carYear = problem_4(inventory);
const oldcar = problem_5(inventory,carYear);
const oldcarlength = oldcar.length;
console.log(oldcar);
console.log(oldcarlength);